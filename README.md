

## RPI4

![](medias/1640702636-1-rpi4-board-1.png)



## Working Images (1. default bookworm SID)

URL: https://raspi.debian.net/tested/20211124_raspi_4_bookworm.img.xz

It needs bit of upgrade and some fix, anyhow it boots.


```` 
/usr/sbin/ntpdate-debian
```` 



```` 
apt-get update


apt-get install ntpdate  wget 

````   


## Working Retropie


It will work ok, likely bit slower content, however it boots well.

URL: https://github.com/RetroPie/RetroPie-Setup/releases/download/4.7.1/retropie-buster-4.7.1-rpi4_400.img.gz



## Gaming on RPI4

Gaming on RPI4 is excellent... 

Example Sauerbraten:

![](medias/1640424992-screenshot.png)


